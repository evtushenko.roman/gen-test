#!/bin/sh

/var/www/bin/console cache:clear --env=prod
/var/www/bin/console cache:clear --env=dev
chown -Rv 1000:1000 /var/www/var/cache
chmod -R 777 /var/www/var/cache