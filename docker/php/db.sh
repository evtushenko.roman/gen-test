#!/bin/sh
sleep 15s

/var/www/bin/console doctrine:database:create --if-not-exists;
/var/www/bin/console doctrine:migrations:migrate --allow-no-migration --quiet