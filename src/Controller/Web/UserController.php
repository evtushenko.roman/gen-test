<?php

namespace App\Controller\Web;

use App\Util\UserService;
use FOS\RestBundle\Controller\Annotations as REST;
use FOS\RestBundle\Controller\AbstractFOSRestController;

class UserController extends AbstractFOSRestController
{
    /**
     * @REST\Route("/user", name="get_all_users", methods={"GET"})
     *
     * @param UserService $userService
     */
    public function getAllUser(UserService $userService)
    {
        $users = $userService->getAllUser();

        dd($users);
    }
}