<?php

namespace App\Controller\Api;

use App\Util\UserService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as REST;


class UserController extends AbstractFOSRestController
{
    /**
     * @REST\Route("/user", name="user_store", methods={"POST"})
     *
     *
     * @param Request $request
     * @param UserService $userService
     */
    public function storeUser(Request $request, UserService $userService)
    {
        $user = $userService->sendUserToQueue($request);

        dd($user);
    }

}