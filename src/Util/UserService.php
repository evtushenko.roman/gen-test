<?php

namespace App\Util;


use App\Entity\User;
use App\Form\UserType;
use Doctrine\ORM\EntityManagerInterface;
use Enqueue\Client\ProducerInterface;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class UserService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var ProducerInterface
     */
    private $producer;
    /**
     * @var AdapterInterface
     */
    private $cache;
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * UserService constructor.
     * @param EntityManagerInterface $em
     * @param AdapterInterface $cache
     * @param ProducerInterface $producer
     * @param FormFactoryInterface $formFactory
     */
    public function __construct(EntityManagerInterface $em, AdapterInterface $cache, ProducerInterface $producer, FormFactoryInterface $formFactory)
    {
        $this->em = $em;
        $this->producer = $producer;
        $this->cache = $cache;
        $this->formFactory = $formFactory;
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function sendUserToQueue(Request $request)
    {
        try {
            $this->producer->sendEvent('storeUser', serialize($request->request->all()));
        } catch (\Exception $exception) {

            return $exception;
            return false;
        }

        return true;
    }

    public function getAllUser()
    {
        $users = $this->em->getRepository(User::class)->findAll();
        $this->em->getConnection()->close();

        return $users;
    }

    public function storeUser($request)
    {
        $user = new User();

        $form = $this->formFactory->create(UserType::class, $user);
        $form->submit($request);

        if ($form->isValid() and $form->isSubmitted()){
            $this->em->persist($user);
            $this->em->flush();
            $this->em->getConnection()->close();

            return true;
        }

        throw new BadRequestHttpException();
    }
}