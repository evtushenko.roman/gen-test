<?php

namespace App\Processor;


use App\Util\UserService;
use Enqueue\Client\TopicSubscriberInterface;
use Interop\Queue\Context;
use Interop\Queue\Message;
use Interop\Queue\Processor;

class StoreUserProcessor implements Processor, TopicSubscriberInterface
{
    /**
     * @var UserService
     */
    private $userService;

    /**
     * StoreUserProcessor constructor.
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function process(Message $message, Context $session)
    {
        dd($message->getBody());
        try{
            $this->userService->storeUser(unserialize($message->getBody()));
        }catch (\Exception $exception){
            return self::REJECT;
        }

        return self::ACK;
    }

    public static function getSubscribedTopics()
    {
        return ['storeUser'];
    }

}